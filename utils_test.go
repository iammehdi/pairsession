package hu

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	p    person
	want string
}

func TestHelloPassing(t *testing.T) {
	assert := assert.New(t)
	tc := testCase{
		p:    person{name: "Mehdi Rami"},
		want: "Hello Mehdi",
	}

	got, _ := tc.p.hello()
	if !assert.Equal(tc.want, got) {
		assert.Failf("want %s; got %s", tc.want, got)
	}

}

func TestHelloFailing(t *testing.T) {
	assert := assert.New(t)
	tc := testCase{
		p:    person{name: "El Mehdi Rami"},
		want: "Hello El Mehdi",
	}

	got, _ := tc.p.hello()
	if !assert.Equal(tc.want, got) {
		assert.Fail(fmt.Sprintf("want %s; got %s", tc.want, got))
	}
}
