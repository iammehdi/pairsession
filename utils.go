package hu

import (
	"fmt"
	"strings"
)

type person struct {
	name string
}

func (p person) hello() (string, error) {
	if p.name == "" {
		return "", fmt.Errorf("name field should be set")
	}
	firstName := strings.Fields(p.name)[0]
	return fmt.Sprintf("Hello %s", firstName), nil
}
